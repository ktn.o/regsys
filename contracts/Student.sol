pragma solidity >=0.4.22<0.6.0;

import './AccountManager.sol';
import './Teacher.sol';
import './Course.sol';

contract Student{
    address private addr;
    bytes32 private email;
    bytes8 private code;
    AccountManager accMan;
    Teacher teacher;
    Course course;
    
    struct student {
        bytes32 fname;
        bytes32 lname;
        bytes8 code;
        uint credit;
        uint gpax;
        bytes32[] enrolledCourseList;
        mapping(bytes8 => uint) grade;
    } 
    
    mapping(bytes32 => bytes8) public _students; // Map address with code.
    
    constructor(address accManAddr, address tAddr, address corsAddr) public{
        accMan = AccountManager(accManAddr);
        teacher = Teacher(tAddr);
        course = Course(corsAddr);
    }
    
    mapping(bytes8 => student) students;
    bytes8[] stdList;
    
    function getAddress() public view returns (address) {
        return address(this);
    }
    
    function addStudent(bytes32 fname, bytes32 lname, bytes8 _code) public {
        bytes32 tName = teacher.teachers(addr);
        require(tName != 0x0, "No permission");
        student storage std = students[_code];
        require(std.fname == 0x0); // Check if studdent not already added.
        std.fname = fname;
        std.lname = lname;
        std.code = _code;
        stdList.push(_code);
    }
 
    function getStudent(bytes8 _code) public view returns (
		bytes32,
		bytes32,
		bytes8,
		uint,
		uint,
		bytes32[] memory) {
                student storage std = students[_code];
                bytes32 tName = teacher.teachers(addr);
                if(code == std.code || tName != 0x0) {
                    return (std.fname, std.lname, std.code, std.credit, std.gpax, std.enrolledCourseList);
                }
    }
    
    function enroll(bytes8 stdCode, bytes8 corsCode, uint grade, uint credit) public {
        student storage std = students[stdCode];
        bytes32 tName = teacher.teachers(addr);
        bytes32 corsLecturer = course.getLecturer(corsCode);
	    require(tName == corsLecturer, "Teacher's name not matched");
	    for(uint i=0; i<std.enrolledCourseList.length; i++) {
		    if(std.enrolledCourseList[i] == corsCode) {
			    revert("course already enrolled.");
		    }
	    }
	    std.enrolledCourseList.push(corsCode);
	    std.grade[corsCode] = grade;
        calcGpax(stdCode, credit, grade);
    }
    
    function getGrade(bytes8 stdCode, bytes8 corsCode) public view returns (uint) {
        student storage std = students[stdCode];
        bytes32 tName = teacher.teachers(addr);
        bytes32 corsLecturer = course.getLecturer(corsCode);
	    require(code == std.code || tName == corsLecturer, "No permission");
        return students[stdCode].grade[corsCode];
    }
    
    function getStdList() public view returns (bytes8[] memory) {
        return stdList;    
    }
    
    function getNumberOfStudents() public view returns (uint) {
        return stdList.length;
    }
    
    function calcGpax(bytes8 _code, uint credit, uint grade) private {
        student storage std = students[_code];
    	uint gpax = std.gpax;
        uint totalCredit = std.credit;
        uint dividend =  gpax*totalCredit;
        uint divisor = totalCredit = std.credit += 3;
        dividend = dividend+(grade*credit);
        uint newGpax = dividend/divisor;
        std.gpax = newGpax;
    }
    
    function authorize(bytes32 _email) public {
        require(_email != 0x0, "Not authorize!");
        addr = accMan.accounts(_email);
        require(addr != address(0x0), "No matched account");
        email = _email;
        bytes memory _code = new bytes(8);
        bytes8 out;
        for(uint i=0; i<8; i++){
            _code[i] = _email[i];
            out |= bytes8(_code[i] & 0xFF) >> (i * 8);
        }
        code = out;
    }
    
}
