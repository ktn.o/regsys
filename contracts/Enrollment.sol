pragma solidity >=0.4.22<0.6.0;

import './Student.sol';
import './Course.sol';

contract Enrollment {
    Student std;
    Course cors;
    
    struct grade {
        bytes8 corsCode;
        uint grade;
    }
    
    mapping(bytes8 => grade) mapGrade;
    
    constructor(address stdAddr, address corsAddr) public {
        std = Student(stdAddr);
        cors = Course(corsAddr);
    }
    
    function getAddress() public view returns (address) {
        return address(this);
    }
    
    function enroll(bytes8 stdCode, bytes8 corsCode, uint _grade, uint credit) public {
	    std.enroll(stdCode, corsCode, _grade, credit);
        cors.enroll(stdCode, corsCode);
    }
}

