pragma solidity >=0.4.22<0.6.0;

import './AccountManager.sol';
import './Teacher.sol';

contract Course {
    address private addr;
    bytes32 private email;
    AccountManager accMan;
    Teacher teacher;
    
    struct course {
        bytes32 name;
        bytes32 lecturer;
        bytes8 code;
        uint credit;
        bytes8 academicYear;
        bytes8[] enrolledStdList;
    }
    
    constructor(address accManAddr, address tAddr) public{
        accMan = AccountManager(accManAddr);
        teacher = Teacher(tAddr);
    }
    
    mapping(bytes8 => course) courses;
    bytes8[] courseList;
    
    function getAddress() public view returns (address) {
        return address(this);
    }
    
    function addCourse(
        bytes32 name,
	    bytes32 lecturer, 
        bytes8 code,
        uint credit,
        bytes8 academicYear) public {
            bytes32 tName = teacher.teachers(addr);
            require(tName == lecturer); // Check if teacher is the owner of the course.
    	    course storage cors = courses[code];
            require(cors.code == 0x0); // Check if course not already added.
            
            cors.name = name;
            cors.lecturer = lecturer;
            cors.code = code;
            cors.credit = credit;
            cors.academicYear = academicYear;
            courseList.push(code);
    }
    
    function enroll(bytes8 stdCode, bytes8 corsCode) public {
    	course storage cors = courses[corsCode];
    	bytes32 tName = teacher.teachers(addr);
    	require(tName == cors.lecturer); // Check if teacher is the owner of the course.
    	
    	for(uint i=0; i<cors.enrolledStdList.length; i++) {
    		if(cors.enrolledStdList[i] == stdCode) {
    			revert("Student already enrolled.");
    		}
    	}	
    	
    	cors.enrolledStdList.push(stdCode);
    }
    
    function getCourseList() public view returns (bytes8[] memory) {
        return courseList;
    }
    
    function getNumberOfCourses() public view returns (uint) {
        return courseList.length;
    }
    
    function getCourse(bytes8 code) public view returns (
        bytes32,
        bytes32,
        bytes8,
        uint,
        bytes8,
        bytes8[] memory) {
		course storage cors = courses[code];
		return (
			cors.name,
        	cors.lecturer,
        	cors.code,
        	cors.credit,
        	cors.academicYear,
        	cors.enrolledStdList
		);
    }
    
    function getLecturer(bytes8 code) public view returns(bytes32) {
        course storage cors = courses[code];
        return cors.lecturer;
    }
    
    function authorize(bytes32 _email) public {
        require(_email != 0x0, "Not authorize!");
        addr = accMan.accounts(_email);
        require(addr != address(0x0), "No matched account");
        email = _email;
    }
}

