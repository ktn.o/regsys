pragma solidity >=0.4.22<0.6.0;

contract AccountManager {
    address public owner;
	mapping(bytes32 => address) public accounts;
	mapping(address => bytes32) private _accounts;
	
	constructor() public {
	    owner = msg.sender;
	}
	
	modifier onlyOwner {
	    require(owner == msg.sender);
	    _;
	}
	
    function getAddress() public view returns (address) {
        return address(this);
    }
    
	function addUser(address addr, bytes32 email) onlyOwner public {
		require(accounts[email] == address(0x0), "This account already registered.");
		require(_accounts[addr] == 0x0, "This account already registered.");
		accounts[email] = addr;
		_accounts[addr] = email;
	}
}
