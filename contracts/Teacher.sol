pragma solidity >=0.4.22<0.6.0;

import './AccountManager.sol';

contract Teacher {
    address private addr;
    bytes32 private email;
    AccountManager accMan;
    
    mapping(address => bytes32) public teachers; // Map address with name.
    
    constructor (address _addr) public {
        accMan = AccountManager(_addr);
    }
    
    function getAddress() public view returns (address) {
        return address(this);
    }
    
    function addTeacher(bytes32 name) public {
        require(teachers[addr] == 0x0, "Already added");
        teachers[addr] = name;
    }
    
    function authorize(bytes32 _email) public {
        require(_email != 0x0, "Not authorize!");
        addr = accMan.accounts(_email);
        require(addr != address(0x0), "No matched account");
        email = _email;
    }
}
