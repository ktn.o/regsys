const Student = artifacts.require('Student')

contract('Student', async accounts => {
	it('should add a student correctly', async () => {
		let stdName = 'std001'
		let stdCode = '000001'
		let name = '0x' + new Buffer(stdName).toString('hex')
		let code = '0x' + new Buffer(stdCode).toString('hex')
		let instance = await Student.deployed()
		let result = await instance.addStudent(name, code, {from: accounts[0]})
		assert(result, 'cannot add a student')
		// console.log(result)
	})	
	it('should get a student info', async () => {
		let code = '0x' + new Buffer('000001').toString('hex')
		let instance = await Student.deployed()
		let result = await instance.getStudent.call(code)
		let name = web3.utils.hexToUtf8(result[0])
		let code_ = web3.utils.hexToUtf8(result[1])
		// console.log(name, code_)
		let bool = name === 'std001' && code_ === '000001'
		assert(bool,'name or code not matched')
	})
	it('should enroll a course successfully', async () => {
		let stdCode = web3.utils.asciiToHex('000001', 8)
		let corsCode = web3.utils.asciiToHex('100000', 8)
		let credit = 3
		let grade = 4
		let instance = await Student.deployed()
		await instance.enroll(stdCode, corsCode, credit, grade, {from: accounts[0]})
		let result = await instance.getStudent.call(stdCode)
		let stdCode_ = web3.utils.hexToUtf8(result[1])
		let corsCode_ = web3.utils.hexToUtf8(result[4][0])
		let credit_ = result[2].toNumber()
		let gpax = result[3].toNumber()
		
		// console.log(stdCode_, corsCode_, credit_, grade)
		// console.log(stdCode, corsCode, credit, grade)
		let bool = stdCode_ === '000001' && corsCode_ === '100000' &&
			 credit === credit_
		assert(bool, 'enroll a course incorrectly')
	})
	it('should get a grade correctly', async () => {
		let stdCode = web3.utils.asciiToHex('000001', 8)
		let corsCode = web3.utils.asciiToHex('100000', 8)
		let instance = await Student.deployed()
		let grade = await instance.getGrade.call(stdCode, corsCode)
		// console.log(result.toNumber())
		assert.equal(grade.toNumber(), 4, 'grade not matched')
	})
	it('should get the list of students', async () => {
		let instance = await Student.deployed()
		let stdList = await instance.getStdList.call()
		// stdList.forEach(index => {
		//	console.log(web3.utils.hexToUtf8(index))
		// })
		let std = web3.utils.hexToUtf8(stdList[0])
		assert.equal(std, '000001', 'student not matched')
	})
	it('should get the number of students', async () => {
		let instance = await Student.deployed()
		let no = await instance.getNumberOfStudents.call()
		// console.log(no.toNumber())
		assert.equal(no.toNumber(), 1, 'number not matched')
	})
})
