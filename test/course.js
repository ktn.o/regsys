const Course = artifacts.require('Course')

contract('Course', async accounts => {
	it('should add a course successfully', async () => {
		let instance = await Course.deployed()
		let name = web3.utils.asciiToHex('Subject Test')
		let lecturer = web3.utils.asciiToHex('Random Guy')
		let code = web3.utils.asciiToHex('111111', 8)
		let credit = 3
		let academicYear = web3.utils.asciiToHex('2019', 4)
		await instance.addCourse(name, lecturer, code, credit, academicYear)
		let course = await instance.getCourse(code)
		let name_ = web3.utils.hexToUtf8(course[0])
		let lecturer_ = web3.utils.hexToUtf8(course[1])
		let code_ = web3.utils.hexToUtf8(course[2])
		let credit_ = course[3].toNumber()
		let academicYear_ = web3.utils.hexToUtf8(course[4])
		let bool = name_ === 'Subject Test' && lecturer_ === 'Random Guy' && code_ === '111111'
		    credit_ === 3 && academicYear_ === '2019'
		// console.log(name, lecturer, code, credit, academicYear)
		// console.log(name_, lecturer_, code_, credit_, academicYear_)
		assert(bool, 'added a course incorrectly')
	})
	it('should enroll successfully', async () => {
		let instance = await Course.deployed()
		let stdCode = web3.utils.asciiToHex('100000')
		let corsCode = web3.utils.asciiToHex('111111')
		await instance.enroll(stdCode, corsCode)
		let course = await instance.getCourse.call(corsCode)
		let std = web3.utils.hexToUtf8(course[5][0])
		assert.equal(std, '100000', 'enrolled unsuccessfully')
	})
	it('should get the course list', async () => {
		let instance = await Course.deployed()
		let code = web3.utils.asciiToHex('111111')
		let result = await instance.getCourseList.call()
		let course = web3.utils.hexToUtf8(result[0])
		assert.equal(course, '111111','course not matched')
	})
	it('should get the number of courses correctly', async () => {
		let instance = await Course.deployed()
		let no = await instance.getNumberOfCourses.call()
		assert.equal(no.toNumber(), '1', 'number not matched')
	})
})
