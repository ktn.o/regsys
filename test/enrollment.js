const Enrollment = artifacts.require('Enrollment')
const Student = artifacts.require('Student')

contract('Enrollment', async accounts => {
	it('should enroll a course correctly', async () => {
		let instance = await Enrollment.deployed()
		let stdCode = web3.utils.asciiToHex('100000', 8)
		let corsCode = web3.utils.asciiToHex('111111', 8)
		let grade = 4
		let credit = 3
		let result = await instance.enroll(stdCode, corsCode, grade, credit)
		let stdInstance = await Student.deployed()
		let std = await stdInstance.getStudent.call(stdCode)
		let course = web3.utils.hexToUtf8(std[4][0])
		assert.equal(course, '111111', 'course not matched')
	})
})
