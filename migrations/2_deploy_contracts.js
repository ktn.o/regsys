let Student = artifacts.require("Student")
let Course = artifacts.require("Course")
let Enrollment = artifacts.require("Enrollment")
let AccountManager = artifacts.require("AccountManager")
let Teacher = artifacts.require("Teacher")

module.exports = async deployer => {
	await deployer.deploy(AccountManager)
	await deployer.deploy(Teacher, AccountManager.address)
	await deployer.deploy(Course, AccountManager.address, Teacher.address)
  	await deployer.deploy(Student, AccountManager.address, Teacher.address, Course.address)
	await deployer.deploy(Enrollment, Student.address, Course.address)
}

