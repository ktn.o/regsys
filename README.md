# regsys
### Usage
```
npm i truffle
truffle compile
truffle migrate
```

##### To test the contracts
example
```
truffle test test/student.js
```

### Documentation
[link](https://gitlab.com/ktn.o/regsys/wikis/home)
